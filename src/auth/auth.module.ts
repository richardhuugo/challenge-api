import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { Users } from 'src/models/users/users.entity';
import { UsersModule } from 'src/models/users/users.module';
import { JwtStrategy } from './jwt.strategy';

@Module({
    imports:[
        UsersModule,
        PassportModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '10h' },
        }),
    ],
    providers: [AuthService,JwtStrategy],
    controllers: [AuthController]
})
export class AuthModule {}
