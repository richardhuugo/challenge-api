import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from 'src/models/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { LoginUsuario } from './dto/LoginUsuario.dto';
import * as bcryptjs from 'bcryptjs';

import { CadastrarUsuario } from './dto/CadastrarUsuario.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(data: LoginUsuario): Promise<Object> {
    const user = await this.userService.findOne({
      where: { email: data.email },
    });

    if (!user)
      throw new HttpException(
        'Usuário ou senha inválidos',
        HttpStatus.BAD_REQUEST,
      );

    const check = await bcryptjs.compareSync(data.password, user.password);

    if (!check)
      throw new HttpException(
        'Usuário ou senha inválidos',
        HttpStatus.BAD_REQUEST,
      );

    return {
      nome: user.nome,
      access_token: this.jwtService.sign({ id: user.id }),
    };
  }

  async cadastro(data: CadastrarUsuario): Promise<Object> {
    const usr = await this.userService.create(data);

    return usr;
  }
}
