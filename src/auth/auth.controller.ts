import { Controller, Post, Body } from '@nestjs/common';
import { LoginUsuario } from './dto/LoginUsuario.dto';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';
import { CadastrarUsuario } from './dto/CadastrarUsuario.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('/login')
  async login(@Body() loginUser: LoginUsuario) {
    return this.authService.login(loginUser);
  }

  @Post('/cadastro')
  async cadastro(@Body() loginUser: CadastrarUsuario) {
    return this.authService.cadastro(loginUser);
  }
}
