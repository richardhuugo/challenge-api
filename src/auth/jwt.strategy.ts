import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from './constants';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/models/users/users.service';
import { AuthenticationError } from 'apollo-server-express';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor( private jwtService: JwtService, private userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: any) {
    const user = await this.userService.findOne({where:{id:payload.id}})
    if(!user)
      throw new AuthenticationError('Faça login novamente');


    return { ...user };
  }

}