import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { validationExceptionFactory } from './config/validation.factory';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: validationExceptionFactory,
    }),
  );

  const options = new DocumentBuilder()
  .setTitle('Focus API')
  .setDescription('Documentação Focus API')
  .setVersion('1.0')  
  .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs-api', app, document);

  await app.listen(3001);
}
bootstrap();
