import { NovaCategoriaInputDTO } from 'src/models/categorias/graphql/input/NovaCategoria.input';

export const CategoriaConfig = {
  create: { many: { disabled: true } },
  update: { disabled: true, many: { disabled: true } },
  delete: { disabled: true, many: { disabled: true } },
};

export const DiasConfig = {
  create: { many: { disabled: true } },
  update: { disabled: true, many: { disabled: true } },
  delete: { disabled: true, many: { disabled: true } },
};
export const TagsConfig = {
  create: { many: { disabled: true } },
  update: { disabled: true, many: { disabled: true } },
  delete: { disabled: true, many: { disabled: true } },
};
export const DesafiosConfig = {
  create: { disabled: true, many: { disabled: true } },
  update: { disabled: true, many: { disabled: true } },
  delete: { disabled: true, many: { disabled: true } },
};
