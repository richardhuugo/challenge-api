import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from './models/users/users.entity';
import { UsersModule } from './models/users/users.module';
import { AuthModule } from './auth/auth.module';
import { CategoriasModule } from './models/categorias/categorias.module';
import { TagsModule } from './models/tags/tags.module';
import { DiasModule } from './models/dias/dias.module';
import { SugestoesModule } from './models/sugestoes/sugestoes.module';
import { DesafiosService } from './models/desafios/desafios.service';
import { DesafiosModule } from './models/desafios/desafios.module';
import { DiasService } from './models/dias/dias.service';
import { DesafioItemsModule } from './models/desafio-items/desafio-items.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Users]),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '127.0.0.1',
      port: 5432,
      username: 'postgres',
      logging: true,
      password: '12345',
      database: 'challenge',
      entities: ['dist/**/*.entity{.ts,.js}'],

      synchronize: true,
    }),
    GraphQLModule.forRoot({
      debug: false,
      playground: true,
      context: ({ req }) => ({ req }),
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
    }),
    UsersModule,
    AuthModule,
    CategoriasModule,
    TagsModule,
    DiasModule,
    SugestoesModule,
    DesafiosModule,
    DesafioItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
