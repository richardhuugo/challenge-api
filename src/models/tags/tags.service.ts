import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { QueryService } from '@nestjs-query/core';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';
import { Tags } from './tags.entity';

@QueryService(Tags)
export class TagsService extends TypeOrmQueryService<Tags> {
  constructor(
    @InjectRepository(Tags)
    private readonly tagsRepository: Repository<Tags>,
  ) {
    super(tagsRepository, { useSoftDelete: true });
  }

  async findByIds(ids: [number]): Promise<any> {
    return this.tagsRepository.findByIds(ids);
  }
  async removerTag(id: number): Promise<String> {
    this.tagsRepository.softDelete(id);

    return 'Removido com sucesso!';
  }
}
