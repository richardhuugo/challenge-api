import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { CRUDResolver } from '@nestjs-query/query-graphql';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { DiasConfig, TagsConfig } from 'src/config/config';
import TagsDTO from './graphql/tags.model';
import { TagsService } from './tags.service';
import { Tags } from './tags.entity';

@Resolver(of => TagsDTO)
export class TagsResolver extends CRUDResolver(TagsDTO, {
  ...TagsConfig,
}) {
  constructor(
    @InjectQueryService(Tags) readonly service: QueryService<TagsDTO>,
    private readonly serviceTag: TagsService,
  ) {
    super(service);
  }
}
