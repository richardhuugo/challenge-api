import { Module } from '@nestjs/common';
import { TagsService } from './tags.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tags } from './tags.entity';
import { TagsResolver } from './tags.resolver';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { argsToArgsConfig } from 'graphql/type/definition';
import { NovaTagInputDTO } from './graphql/input/NovaTag.input';
import TagsDTO from './graphql/tags.model';
import { TagsConfig } from 'src/config/config';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Tags])],
      resolvers: [
        {
          DTOClass: TagsDTO,
          EntityClass: Tags,
          CreateDTOClass: NovaTagInputDTO,
          ...TagsConfig,
        },
      ],
    }),
  ],
  providers: [TagsService, TagsResolver],
  exports: [TagsService],
})
export class TagsModule {}
