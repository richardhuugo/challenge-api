import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';
import { Length } from 'class-validator';

@ObjectType()
export default class TagsDTO {
  @FilterableField(() => ID)
  id: number;

  @Length(100)
  @FilterableField()
  tag: string;

  @Field(() => GraphQLISODateTime)
  created_at!: Date;

  @Field(() => GraphQLISODateTime)
  updated_at!: Date;
}
