import { Field, InputType } from '@nestjs/graphql';
import { IsString, Length } from 'class-validator';

@InputType('NovaTagInput')
export class NovaTagInputDTO {
  @Field()
  @IsString()
  // min length of 5 and max of 5 characters
  @Length(5, 100)
  tag!: string;
}
