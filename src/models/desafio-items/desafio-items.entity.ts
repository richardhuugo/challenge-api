import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Length } from 'class-validator';
import { Desafios } from '../desafios/desafios.entity';
import { Users } from '../users/users.entity';

const enum Status {
  GRAY = 'GRAY',
  RED = 'RED',
  GREEN = 'GREEN',
}

@Entity()
export class DesafioItems {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: Status;

  @Column({ nullable: true })
  descricao?: string;

  @Column({ nullable: true })
  foto?: string;

  @Column({ nullable: true })
  video?: string;

  @CreateDateColumn({
    type: 'date',
  })
  data: Date;

  @ManyToOne(type => Desafios, { cascade: true })
  @JoinColumn({ name: 'desafio_id' })
  desafio_id: Desafios;

  @ManyToOne(type => Users, { cascade: true })
  @JoinColumn({ name: 'user_id' })
  user_id: Users;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;
}
