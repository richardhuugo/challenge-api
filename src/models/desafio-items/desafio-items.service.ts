import { Injectable } from '@nestjs/common';
import { DesafioItems } from './desafio-items.entity';
import { QueryService } from '@nestjs-query/core';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DesafioItemsService {
  constructor(
    @InjectRepository(DesafioItems)
    private readonly desafioItemService: Repository<DesafioItems>,
  ) {}

  async incluirVarios(data: DesafioItems): Promise<any> {
    return this.desafioItemService.insert(data);
  }
}
