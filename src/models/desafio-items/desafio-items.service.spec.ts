import { Test, TestingModule } from '@nestjs/testing';
import { DesafioItemsService } from './desafio-items.service';

describe('DesafioItemsService', () => {
  let service: DesafioItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DesafioItemsService],
    }).compile();

    service = module.get<DesafioItemsService>(DesafioItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
