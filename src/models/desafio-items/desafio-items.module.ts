import { Module } from '@nestjs/common';
import { DesafioItemsService } from './desafio-items.service';
import { DesafioItems } from './desafio-items.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([DesafioItems])],
  providers: [DesafioItemsService],
  exports: [DesafioItemsService],
})
export class DesafioItemsModule {}
