import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Desafios } from './desafios.entity';
import { DesafiosService } from './desafios.service';
import { DesafiosResolver } from './desafios.resolver';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { NovoDesafioDTO } from './graphql/input/NovoDesafio.input';
import { DesafiosConfig } from 'src/config/config';
import { Tags } from '../tags/tags.entity';
import { TagsService } from '../tags/tags.service';
import { TagsModule } from '../tags/tags.module';
import { DesafiosDTO } from './graphql/desafios.model';
import { DiasModule } from '../dias/dias.module';
import { DesafioItemsModule } from '../desafio-items/desafio-items.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TagsModule,
    DiasModule,
    DesafioItemsModule,
    UsersModule,
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Desafios])],
      resolvers: [
        {
          DTOClass: DesafiosDTO,
          EntityClass: Desafios,
          CreateDTOClass: NovoDesafioDTO,
          ...DesafiosConfig,
        },
      ],
    }),
  ],
  providers: [DesafiosService, DesafiosResolver],
  exports: [DesafiosService],
})
export class DesafiosModule {}
