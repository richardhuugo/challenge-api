import { Field, InputType, GraphQLISODateTime } from '@nestjs/graphql';
import {
  IsString,
  Length,
  IsDate,
  IsBoolean,
  IsNumber,
  IsOptional,
} from 'class-validator';

@InputType('NovoDesafio')
export class NovoDesafioDTO {
  @Field()
  @IsString()
  @Length(5, 100)
  titulo!: string;

  @Field()
  @IsString()
  @Length(5, 100)
  objetivo!: string;

  @Field(() => GraphQLISODateTime)
  @IsDate()
  data_inicio: Date;

  @Field(() => GraphQLISODateTime, {
    defaultValue: () => 'CURRENT_TIMESTAMP(6)',
  })
  @IsDate()
  lembrete: Date;

  @Field({ defaultValue: false })
  @IsBoolean()
  grupo: Boolean;

  @Field({ defaultValue: false })
  @IsBoolean()
  privado: Boolean;

  @Field()
  @IsNumber()
  categoria_id!: number;

  @Field()
  @IsNumber()
  dia_id!: number;

  @Field({ nullable: true })
  @IsOptional()
  sugestao_id: number;

  @Field(() => [Number])
  @IsOptional()
  desafio_tags: [number];

  @Field(() => [Number])
  @IsOptional()
  desafio_friends: [number];
}
