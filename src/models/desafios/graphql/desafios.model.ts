import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import * as bcryptjs from 'bcryptjs';

import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';
import { Length } from 'class-validator';
import UsersDTO from 'src/models/users/graphql/users.model';
import TagsDTO from 'src/models/tags/graphql/tags.model';
import SugestoesDTO from 'src/models/sugestoes/graphql/sugestoes.model';
import DiasDTO from 'src/models/dias/graphql/dias.model';
import CategoriasDTO from 'src/models/categorias/graphql/categorias.model';
import { Tags } from 'src/models/tags/tags.entity';
import { Desafios } from '../desafios.entity';

@ObjectType('desafio')
export class DesafiosDTO {
  @FilterableField(() => ID)
  id: number;

  @Length(100)
  @FilterableField()
  titulo: string;

  @Field()
  objetivo: string;

  @Field(() => GraphQLISODateTime, { nullable: true })
  data_inicio?: Date;

  @Field({ defaultValue: false })
  grupo?: Boolean;

  @Field({ defaultValue: false })
  privado?: Boolean;

  @Field(type => CategoriasDTO)
  categoria_id: CategoriasDTO;

  @Field(type => UsersDTO)
  user_id: UsersDTO;

  @Field(type => DiasDTO)
  dia_id: DiasDTO;

  @Field(type => SugestoesDTO)
  sugestao_id: SugestoesDTO;

  @Field(type => [TagsDTO])
  desafio_tags: [TagsDTO];

  @Field(type => DesafiosDTO)
  relacionado: DesafiosDTO;

  @Field()
  public created_at: Date;

  @Field()
  public updated_at: Date;

  @Field()
  deleted_at?: Date;
}
