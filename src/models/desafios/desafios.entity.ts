import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import * as bcryptjs from 'bcryptjs';
import { Length } from 'class-validator';
import { Categorias } from '../categorias/categorias.entity';
import { Users } from '../users/users.entity';
import { Dias } from '../dias/dias.entity';
import { Sugestoes } from '../sugestoes/sugestoes.entity';
import { Tags } from '../tags/tags.entity';
import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@Entity()
export class Desafios {
  constructor(partial: Partial<any>) {
    Object.assign(this, partial);
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  titulo: string;

  @Column()
  objetivo: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
  data_inicio: Date;

  @CreateDateColumn({
    type: 'timestamp',
    default: null,
    nullable: true,
  })
  lembrete: Date;

  @Column({ default: () => false })
  grupo: Boolean;

  @Column({ default: () => false })
  privado: Boolean;

  @ManyToOne(type => Categorias, { nullable: false })
  @JoinColumn({ name: 'categoria_id' })
  categoria_id: Categorias;

  @ManyToOne(type => Users, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  user_id: Users;

  @ManyToOne(type => Dias, { nullable: false })
  @JoinColumn({ name: 'dia_id' })
  dia_id: Dias;

  @ManyToOne(type => Sugestoes, { nullable: true })
  @JoinColumn({ name: 'sugestao_id' })
  sugestao_id: Sugestoes;

  @ManyToOne(type => Desafios, { nullable: true })
  @JoinColumn({ name: 'relacionado' })
  relacionado: Desafios;

  @ManyToMany(type => Tags)
  @JoinTable({
    name: 'desafio_tag',
    joinColumn: {
      name: 'desafio_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'tag_id',
      referencedColumnName: 'id',
    },
  })
  desafio_tags: Tags[];

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;
}
