import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { QueryService } from '@nestjs-query/core';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';
import { Desafios } from './desafios.entity';
import { NovoDesafioDTO } from './graphql/input/NovoDesafio.input';
import { Tags } from '../tags/tags.entity';
import { TagsService } from '../tags/tags.service';
import { Users } from '../users/users.entity';
import { DiasService } from '../dias/dias.service';
import { DesafioItemsService } from '../desafio-items/desafio-items.service';
import { UsersService } from '../users/users.service';

@QueryService(Desafios)
export class DesafiosService extends TypeOrmQueryService<Desafios> {
  constructor(
    @InjectRepository(Desafios)
    private readonly desafiosRepository: Repository<Desafios>,
    private readonly tagService: TagsService,
    private readonly diasService: DiasService,
    private readonly desafioItemService: DesafioItemsService,
    private readonly usersService: UsersService,
  ) {
    super(desafiosRepository, { useSoftDelete: true });
  }

  async createOneDesafio(data: NovoDesafioDTO, user: Users): Promise<Desafios> {
    const novoDesafio = new Desafios({ ...data, user_id: user });
    novoDesafio.desafio_tags = await this.tagService.findByIds(
      data.desafio_tags,
    );
    const desafioCreated = await this.desafiosRepository.save(novoDesafio);
    const checkDias = await this.diasService.findDia(data.dia_id);

    const Items = await this.CalularItem(
      checkDias.dia,
      data.data_inicio,
      desafioCreated.id,
      user.id,
    );
    console.log(data.desafio_friends);

    // incluir os items do desafio do usuario com base na quantidade de dias
    await this.desafioItemService.incluirVarios(Items);

    //return desafioCreated
    return await this.desafiosRepository.findOne({
      where: {
        user_id: 1,
      },
    });
  }

  async incluirAmigos(
    friendsDesafio: [number],
    desafio_id: number,
    dataDesafio: any,
  ) {
    if (friendsDesafio.length <= 0) return false;

    friendsDesafio.map(async friend => {
      const found = await this.usersService.findOne({ where: { id: friend } });
    });
  }
  async CalularItem(
    quantidade: number,
    data_inicio: Date,
    desafio_id: number,
    user_id: number,
  ): Promise<any> {
    const item = [];
    const data = new Date(data_inicio);
    const datasInclusas = [];
    Array.from(Array(quantidade), (_, i) => i + 1).map(ItemMaped => {
      data.setDate(data.getDate() + 1);
      item.push({
        data: new Date(`${data}`),
        status: 'GRAY',
        user_id,
        desafio_id,
      });
    });

    return item;
  }

  async meusDesafios(users: Users): Promise<Desafios[]> {
    return await this.desafiosRepository.find({ where: { user_id: users.id } });
  }
  async detalhesDesafio(id: number, users: Users): Promise<Desafios> {
    return await this.desafiosRepository.findOne({
      where: {
        id,
        user_id: users.id,
      },
    });
  }
  async removerDesafio(id: number): Promise<String> {
    this.desafiosRepository.softDelete(id);

    return 'Removido com sucesso!';
  }
}
