import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { CRUDResolver } from '@nestjs-query/query-graphql';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { DiasConfig, DesafiosConfig } from 'src/config/config';

import { Desafios } from './desafios.entity';
import { DesafiosService } from './desafios.service';
import { NovoDesafioDTO } from './graphql/input/NovoDesafio.input';
import { Tags } from '../tags/tags.entity';
import { DesafiosDTO } from './graphql/desafios.model';
import { GqlAuthGuard } from 'src/auth/GqlAuthGuard.guard';
import { Users } from '../users/users.entity';

const CurrentUsers = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);

@Resolver(of => Desafios)
export class DesafiosResolver extends CRUDResolver(DesafiosDTO, {
  ...DesafiosConfig,
}) {
  constructor(
    @InjectQueryService(Desafios) readonly service: QueryService<DesafiosDTO>,
    private readonly serviceDesafio: DesafiosService,
  ) {
    super(service);
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(returns => DesafiosDTO)
  async createOneDesafio(
    @CurrentUsers() users: Users,
    @Args('data') data: NovoDesafioDTO,
  ): Promise<Desafios> {
    return this.serviceDesafio.createOneDesafio(data, users);
  }

  @UseGuards(GqlAuthGuard)
  @Query(returns => [DesafiosDTO])
  async meusDesafios(@CurrentUsers() users: Users): Promise<Desafios[]> {
    return this.serviceDesafio.meusDesafios(users);
  }

  @UseGuards(GqlAuthGuard)
  @Query(returns => DesafiosDTO)
  async detalharDesafio(
    @Args('id') id: number,
    @CurrentUsers() users: Users,
  ): Promise<Desafios> {
    return this.serviceDesafio.detalhesDesafio(id, users);
  }
}
