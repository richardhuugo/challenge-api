import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@ObjectType('Categoria')
export default class CategoriasDTO {
  @FilterableField(() => ID)
  id: number;

  @FilterableField()
  categoria: string;

  @Field(() => GraphQLISODateTime)
  created_at!: Date;

  @Field(() => GraphQLISODateTime)
  updated_at!: Date;
}
