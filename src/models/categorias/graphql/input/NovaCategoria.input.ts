import { Field, InputType } from '@nestjs/graphql';
import { IsString, Length } from 'class-validator';

@InputType('NovaCategoriaInput')
export class NovaCategoriaInputDTO {
  @Field()
  @IsString()
  // min length of 5 and max of 5 characters
  @Length(5, 100)
  categoria!: string;
}
