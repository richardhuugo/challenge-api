import { Module } from '@nestjs/common';
import { CategoriasService } from './categorias.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Categorias } from './categorias.entity';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import CategoriasDTO from './graphql/categorias.model';
import { NovaCategoriaInputDTO } from './graphql/input/NovaCategoria.input';
import { CategoriasResolver } from './categorias.resolver';
import { CategoriaConfig } from 'src/config/config';
@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Categorias])],
      resolvers: [
        {
          DTOClass: CategoriasDTO,
          EntityClass: Categorias,
          CreateDTOClass: NovaCategoriaInputDTO,
          ...CategoriaConfig,
        },
      ],
    }),
  ],
  providers: [CategoriasService, CategoriasResolver],
  exports: [CategoriasService],
})
export class CategoriasModule {}
