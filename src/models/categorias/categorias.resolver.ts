import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import CategoriasDTO from './graphql/categorias.model';
import { CRUDResolver } from '@nestjs-query/query-graphql';
import { Categorias } from './categorias.entity';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { CategoriaConfig } from 'src/config/config';
import { CategoriasService } from './categorias.service';

@Resolver(of => CategoriasDTO)
export class CategoriasResolver extends CRUDResolver(CategoriasDTO, {
  ...CategoriaConfig,
}) {
  constructor(
    @InjectQueryService(Categorias)
    readonly service: QueryService<CategoriasDTO>,
    private readonly serviceCategoria: CategoriasService,
  ) {
    super(service);
  }

  @Mutation(returns => String)
  async deleteOneCategoria(@Args('id') id: number): Promise<String> {
    return this.serviceCategoria.removerCategoria(id);
  }
}
