import { Injectable, Query } from '@nestjs/common';
import { Categorias } from './categorias.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { QueryService } from '@nestjs-query/core';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';

@QueryService(Categorias)
export class CategoriasService extends TypeOrmQueryService<Categorias> {
  constructor(
    @InjectRepository(Categorias)
    private readonly userRepository: Repository<Categorias>,
  ) {
    super(userRepository, { useSoftDelete: true });
  }

  async removerCategoria(id: number): Promise<String> {
    this.userRepository.softDelete(id);

    return 'Removido com sucesso!';
  }
}
