import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { QueryService } from '@nestjs-query/core';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';
import { Dias } from './dias.entity';

@QueryService(Dias)
export class DiasService extends TypeOrmQueryService<Dias> {
  constructor(
    @InjectRepository(Dias)
    private readonly diasRepository: Repository<Dias>,
  ) {
    super(diasRepository, { useSoftDelete: true });
  }

  async findDia(id: number): Promise<Dias> {
    return this.diasRepository.findOne({ where: { id } });
  }
  async removerDia(id: number): Promise<String> {
    this.diasRepository.softDelete(id);

    return 'Removido com sucesso!';
  }
}
