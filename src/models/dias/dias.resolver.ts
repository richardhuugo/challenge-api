import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { CRUDResolver } from '@nestjs-query/query-graphql';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { DiasConfig } from 'src/config/config';

import DiasDTO from './graphql/dias.model';
import { Dias } from './dias.entity';
import { DiasService } from './dias.service';

@Resolver(of => DiasDTO)
export class DiasResolver extends CRUDResolver(DiasDTO, {
  ...DiasConfig,
}) {
  constructor(
    @InjectQueryService(Dias) readonly service: QueryService<DiasDTO>,
    private readonly serviceCategoria: DiasService,
  ) {
    super(service);
  }
}
