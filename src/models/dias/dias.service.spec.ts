import { Test, TestingModule } from '@nestjs/testing';
import { DiasService } from './dias.service';

describe('DiasService', () => {
  let service: DiasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DiasService],
    }).compile();

    service = module.get<DiasService>(DiasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
