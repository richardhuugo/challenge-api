import { Field, InputType } from '@nestjs/graphql';
import { IsString, Length, IsNumber } from 'class-validator';

@InputType('NovoDiaInput')
export class NovoDiaInputDTO {
  @Field()
  @IsNumber()
  dia: number;
}
