import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@ObjectType('Dia')
export default class DiasDTO {
  @FilterableField(() => ID)
  id: number;

  @FilterableField()
  dia: number;

  @Field(() => GraphQLISODateTime)
  created_at!: Date;

  @Field(() => GraphQLISODateTime)
  updated_at!: Date;

  @Field(() => GraphQLISODateTime)
  deleted_at: Date;
}
