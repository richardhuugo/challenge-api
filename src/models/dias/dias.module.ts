import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dias } from './dias.entity';
import { DiasService } from './dias.service';
import { DiasResolver } from './dias.resolver';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import DiasDTO from './graphql/dias.model';
import { NovoDiaInputDTO } from './graphql/input/NovoDia.input';
import { DiasConfig } from 'src/config/config';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Dias])],
      resolvers: [
        {
          DTOClass: DiasDTO,
          EntityClass: Dias,
          CreateDTOClass: NovoDiaInputDTO,
          ...DiasConfig,
        },
      ],
    }),
  ],
  providers: [DiasService, DiasResolver],
  exports: [DiasService],
})
export class DiasModule {}
