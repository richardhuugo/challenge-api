import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import SugestoesDTO from './graphql/sugestoes.model';
import { GqlAuthGuard } from 'src/auth/GqlAuthGuard.guard';

const pubSub = new PubSub();
const CurrentUsers = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);

@Resolver(of => SugestoesDTO)
export class SugestoesResolver {}
