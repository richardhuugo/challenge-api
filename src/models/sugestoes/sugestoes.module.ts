import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Sugestoes } from './sugestoes.entity';
import { SugestoesService } from './sugestoes.service';
import { SugestoesResolver } from './sugestoes.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([Sugestoes])],
  providers: [SugestoesService, SugestoesResolver],
  exports: [SugestoesService],
})
export class SugestoesModule {}
