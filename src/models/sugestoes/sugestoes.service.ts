import { Injectable } from '@nestjs/common';
import { Sugestoes } from './sugestoes.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SugestoesService {
  constructor(
    @InjectRepository(Sugestoes)
    private readonly userRepository: Repository<Sugestoes>,
  ) {}
}
