import { QueryService } from '@nestjs-query/core';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';

import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BuscarUsuarios } from './graphql/dto/BuscarUsuarios.args';
import { Repository } from 'typeorm';
import { Users } from './users.entity';
import { JwtService } from '@nestjs/jwt';
import { AuthenticationError, ApolloError } from 'apollo-server-express';
import { CadastrarUsuario } from 'src/auth/dto/CadastrarUsuario.dto';
import { NovoSeguidor } from './graphql/dto/NovoSeguidor.args';

@QueryService(Users)
export class UsersService extends TypeOrmQueryService<Users> {
  constructor(
    @InjectRepository(Users)
    private readonly userRepository: Repository<Users>,
  ) {
    super(userRepository, { useSoftDelete: true });
  }

  /**
   * MOCK
   * https://rohanfaiyaz.com/post/nest-graphql/
   * Put some real business logic here
   * Left for demonstration purposes
   */

  async create(data: CadastrarUsuario): Promise<Users> {
    const user = new Users();
    user.nome = data.nome;
    user.email = data.email;
    user.password = data.password;

    await this.userRepository.save(user);
    return user;
  }

  async findOneById(id: number): Promise<Users> {
    return await this.userRepository.findOne({ where: { id } });
  }
  async findOne(options?: object): Promise<Users> {
    return this.userRepository.findOne(options);
  }

  async findAll(userArgs: BuscarUsuarios): Promise<Users[]> {
    return [] as Users[];
  }

  async remove(id: string): Promise<boolean> {
    return true;
  }

  async novoSeguidor(meid: number, data: NovoSeguidor) {
    const novoSeguidor = await this.userRepository.findOne(meid, {
      relations: ['seguidores'],
    });
    const friendid = await this.findOneById(data.friendid);
    novoSeguidor.seguidores.push(friendid);
    const repository = await this.userRepository.save(novoSeguidor);
    return friendid;
  }
}
