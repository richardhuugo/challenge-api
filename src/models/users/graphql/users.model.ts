import { Field, ObjectType, ID, GraphQLISODateTime } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';

@ObjectType()
export default class UsersDTO {
  @FilterableField(() => ID)
  id: number;

  @FilterableField()
  nome: string;

  @Field()
  email: string;

  @Field()
  foto: string;

  @FilterableField({ nullable: true })
  nickname: string;

  @Field()
  password: string;

  @Field({ nullable: true })
  provider: string;

  @Field({ nullable: true })
  provider_id: string;

  @Field(() => GraphQLISODateTime)
  created_at!: Date;

  @Field(() => GraphQLISODateTime)
  updated_at!: Date;

  @Field(() => GraphQLISODateTime)
  deleted_at?: Date;
}
