import { ArgsType, Field, Int } from '@nestjs/graphql';

@ArgsType()
export class NovoSeguidor {
  @Field(type => Int)
  friendid;
}
