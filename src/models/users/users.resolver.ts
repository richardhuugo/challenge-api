import { NotFoundException, UseGuards } from '@nestjs/common';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { UsersService } from './users.service';

import UsersDTO from './graphql/users.model';
import { GqlAuthGuard } from 'src/auth/GqlAuthGuard.guard';
import { BuscarUsuarios } from './graphql/dto/BuscarUsuarios.args';
import { NovoSeguidor } from './graphql/dto/NovoSeguidor.args';

const pubSub = new PubSub();
const CurrentUsers = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);

@Resolver(of => UsersDTO)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(GqlAuthGuard)
  @Query(returns => UsersDTO)
  async me(@CurrentUsers() users: UsersDTO): Promise<UsersDTO> {
    console.log(users);
    const user = await this.usersService.findOneById(users.id);
    if (!user) {
      throw new NotFoundException(users.id);
    }
    return user;
  }

  @Query(returns => [UsersDTO])
  users(@Args() buscarUsers: BuscarUsuarios): Promise<UsersDTO[]> {
    return this.usersService.findAll(buscarUsers);
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(returns => UsersDTO)
  seguir(
    @CurrentUsers() users: UsersDTO,
    @Args() novoSeguidor: NovoSeguidor,
  ): any {
    return this.usersService.novoSeguidor(users.id, novoSeguidor);
  }

  // @Subscription(returns => Users)
  // novoUsuarioAdicionado() {
  //   return pubSub.asyncIterator('novoUsuarioCadastrado');
  // }
}
