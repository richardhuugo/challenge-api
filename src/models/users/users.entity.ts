/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import * as bcryptjs from 'bcryptjs';
import { IsEmail, Length } from 'class-validator';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Length(100)
  @Column({ length: 100 })
  nome: string;

  @Length(50)
  @Column({ length: 50, unique: true })
  @IsEmail()
  email: string;

  @Length(255)
  @Column({ nullable: true, length: 255 })
  foto: string;

  @Column({ nullable: true, length: 20 })
  @Length(20)
  nickname: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  provider: string;

  @Column({ nullable: true })
  provider_id: string;

  @ManyToMany(type => Users)
  @JoinTable({
    name: 'seguidores',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'seguidor_id',
      referencedColumnName: 'id',
    },
  })
  seguidores: Users[];

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @BeforeInsert()
  async hashPassword() {
    this.password = await await bcryptjs.hash(this.password, 5);
  }
}
